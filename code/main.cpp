#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define Assert(Condition) if(!(Condition)) { *(int *)0 = 0; }
typedef int32_t bool32;

struct file_result
{
	uint8_t *FileContents;
	uint32_t FileSize;
};

static struct file_result
ReadEntireFileIntoMemoryAndNullTerminate(uint8_t *FileName)
{
	struct file_result Result = {0};

	FILE *File = fopen((char *)FileName, "rb");
	Assert(File != 0);
	if(File != 0)
	{
		uint32_t FileSize = 0;
		fseek(File, 0, SEEK_END);
		FileSize = ftell(File);
		fseek(File, 0, SEEK_SET);

		uint8_t *FileContents = 0;
		FileContents = (uint8_t *)malloc(sizeof(uint8_t) * FileSize + 1);
		Assert(FileContents != 0);

		uint32_t BytesRead = fread(FileContents, 1, FileSize, File);
		if(BytesRead == FileSize)
		{
			FileContents[FileSize] = 0;
			Result.FileContents = FileContents;
			Result.FileSize = FileSize;
		}

		fclose(File);
	}

	return(Result);
}

static void
ReleaseFileResult(struct file_result *FileResult)
{
	if((FileResult != 0) && (FileResult->FileSize != 0))
	{
		free(FileResult->FileContents);
		FileResult->FileSize = 0;
	}
}

static inline bool32
IsLineEnding(uint8_t Character)
{
	bool32 Result = ( (Character == '\r') ||
					  (Character == '\n') );
	return(Result);
}

static inline void
SwapStringPointers(uint8_t **Left, uint8_t **Right)
{
	uint8_t *Temp = *Right;
	*Right = *Left;
	*Left = Temp;
}

static inline int32_t
FileNameCompare(uint8_t *Left, uint8_t *Right)
{
	for(; *Left == *Right; ++Left, ++Right)
	{
		if((*Left == 0) ||
		   (*Left == ':'))
		{
			return 0;
		}
	}

	return(*Left - *Right);
}

static inline int32_t
RowCompare(uint8_t *Left, uint8_t *Right, int32_t FieldsToCompare = 1)
{
	for(; (*Left == *Right) && (FieldsToCompare != 0); ++Left, ++Right)
	{
		if(*Left == ':')
		{
			--FieldsToCompare;
		}
	}

	return(*Left - *Right);
}

static inline void
QuickSort(uint8_t **Array, int32_t Lower, int32_t Upper)
{
	if(Lower >= Upper)
	{
		return;
	}

	uint8_t *PartitionValue = Array[Lower];
	int32_t PartitionIndex = Lower;
	for(int32_t TestIndex = Lower + 1; TestIndex <= Upper; ++TestIndex)
	{
		if(RowCompare(Array[TestIndex], PartitionValue) < 0)
		{
			SwapStringPointers(&Array[++PartitionIndex], &Array[TestIndex]);
		}
	}
	SwapStringPointers(&Array[Lower], &Array[PartitionIndex]);

	QuickSort(Array, Lower, PartitionIndex - 1);
	QuickSort(Array, PartitionIndex + 1, Upper);
}

int
main(int32_t ArgCount, uint8_t **Args)
{
	if(ArgCount != 2)
	{
		printf("Usage: %s [Input File]\n", Args[0]);
		return(1);
	}

	struct file_result InputFile = ReadEntireFileIntoMemoryAndNullTerminate(Args[1]);
	if(InputFile.FileSize == 0)
	{
		fprintf(stderr, "Failed to open '%s' for reading.", Args[1]);
		return(2);
	}

	// NOTE(rick): Initial array size is set to 4k (1 page) since malloc will
	// give us a page anyway. The array size is allowed to grow as needed.
	uint32_t MaxSuffixArraySize = 4096;
	uint8_t **FileData = (uint8_t **)malloc(sizeof(uint8_t *) * MaxSuffixArraySize);
	Assert(FileData != 0);

	int32_t RowsInserted = 0;
	uint8_t *Character = InputFile.FileContents;
	while(*Character != 0)
	{
		if(IsLineEnding(*Character))
		{
			while(IsLineEnding(*Character))
			{
				*Character++ = 0;
			}

			FileData[RowsInserted++] = Character;
			
			if(RowsInserted >= MaxSuffixArraySize)
			{
				MaxSuffixArraySize <<= 1;
				uint8_t **NewFileData = (uint8_t **)realloc(FileData, sizeof(uint8_t *) * MaxSuffixArraySize);
				// NOTE(rick):  If we can't grow the array just die
				Assert(NewFileData);
				FileData = NewFileData;
			}
		}

		++Character;
	}

	QuickSort(FileData, 0, RowsInserted - 1);
#if 0
	// NOTE(rick): Enable this to validate QuickSort results
	for(int32_t i = 0; i < RowsInserted-2; ++i)
	{
		Assert(FileNameCompare(FileData[i], FileData[i+1]) <= 0);
	}
#endif

	for(int32_t Row = 0; Row < RowsInserted - 1;)
	{
		int32_t CompareRow = Row + 1;
		if(RowCompare(FileData[Row], FileData[CompareRow], 2) == 0)
		{
			printf("%s\n", FileData[Row]);
			while(RowCompare(FileData[Row], FileData[CompareRow], 2) == 0)
			{
				printf("%s\n", FileData[CompareRow]);
				++CompareRow;
			}
		}

		Row = CompareRow;
	}

	ReleaseFileResult(&InputFile);
	free(FileData);

	return 0;
}
