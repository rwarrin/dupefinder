@echo off

REM SET CompilerOptions=/nologo /Zi /fp:fast /Wall /W1
SET LinkerOptions=/incremental:no

REM Release Settings
SET CompilerOptions=/nologo /Oi /Ox /fp:fast /favor:INTEL64 /w /MT

if not exist ..\build mkdir ..\build
pushd ..\build

cl.exe %CompilerOptions% ..\code\main.cpp /link %LinkerOptions%

popd
