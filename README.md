DupeFinder - Finds duplicate rows of text within a file.


How to use it:

`main.exe [filename]` where filename is the file to parse.

Expected file format: string:string:string<newline>


How to generate the input file:

Windows

Using command prompt navigate to the root directory where you want to search for duplicates

run `forfiles /S /C "cmd /c echo @file:@fsize:@path" > filename`

How it works:

Read file

Create suffix array with pointers to each line in the file

Sort the array of pointers on the first string field

Compare the sorted array of pointers on the first and second string fields and report duplicates.


How to build:

Run `build.bat` in the code directory.
